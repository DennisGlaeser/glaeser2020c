// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief main file for scenario 1
 */
#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

#include "properties_bulk.hh"
#include "properties_facet.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/assembly/diffmethod.hh>
#include <dumux/linear/seqsolverbackend.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/porousmediumflow/velocityoutput.hh>

#include "pressuresolve.hh"

// The type tags used here
using BulkTypeTag = Dumux::Properties::TTag::BULKTYPETAG;
using FacetTypeTag = Dumux::Properties::TTag::FACETTYPETAG;

using BulkIncompTypeTag = Dumux::Properties::TTag::BULKINCOMPTYPETAG;
using FacetIncompTypeTag = Dumux::Properties::TTag::FACETINCOMPTYPETAG;

// obtain/define some types to be used below in the property definitions and in main
template<class BulkTT, class FacetTT>
class ScenarioOneTestTraits
{
    using BulkFVGridGeometry = Dumux::GetPropType<BulkTT, Dumux::Properties::GridGeometry>;
    using FacetFVGridGeometry = Dumux::GetPropType<FacetTT, Dumux::Properties::GridGeometry>;
public:
    using MDTraits = Dumux::MultiDomainTraits<BulkTT, FacetTT>;
    using CouplingMapper = Dumux::FacetCouplingMapper<BulkFVGridGeometry, FacetFVGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingManager<MDTraits, CouplingMapper>;
};

// set the coupling manager property in all sub-problems
namespace Dumux {
namespace Properties {

using Traits = ScenarioOneTestTraits<BulkTypeTag, FacetTypeTag>;
using IncompressibleTraits = ScenarioOneTestTraits<BulkIncompTypeTag, FacetIncompTypeTag>;

template<class TypeTag>
struct CouplingManager<TypeTag, BulkTypeTag> { using type = typename Traits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, FacetTypeTag> { using type = typename Traits::CouplingManager; };

template<class TypeTag>
struct CouplingManager<TypeTag, BulkIncompTypeTag> { using type = typename IncompressibleTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, FacetIncompTypeTag> { using type = typename IncompressibleTraits::CouplingManager; };

} // end namespace Properties
} // end namespace Dumux

/*!
 * \brief Updates the finite volume grid geometry for the box scheme.
 *
 * This is necessary as the finite volume grid geometry for the box scheme with
 * facet coupling requires additional data for the update. The reason is that
 * we have to create additional faces on interior boundaries, which wouldn't be
 * created in the standard scheme.
 */
template< class GridGeometry,
          class GridManager,
          class FacetGridView,
          std::enable_if_t<GridGeometry::discMethod == Dumux::DiscretizationMethod::box, int> = 0 >
void updateBulkFVGridGeometry(GridGeometry& gridGeometry,
                              const GridManager& gridManager,
                              const FacetGridView& facetGridView)
{
    using BulkFacetGridAdapter = Dumux::CodimOneGridAdapter<typename GridManager::Embeddings>;
    BulkFacetGridAdapter facetGridAdapter(gridManager.getEmbeddings());
    gridGeometry.update(facetGridView, facetGridAdapter);
}

/*!
 * \brief Updates the finite volume grid geometry for the cell-centered schemes.
 */
template< class GridGeometry,
          class GridManager,
          class FacetGridView,
          std::enable_if_t<GridGeometry::discMethod != Dumux::DiscretizationMethod::box, int> = 0 >
void updateBulkFVGridGeometry(GridGeometry& gridGeometry,
                              const GridManager& gridManager,
                              const FacetGridView& facetGridView)
{
    gridGeometry.update();
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    // initialize MPI, finalize is done automatically on exit
    Dune::MPIHelper::instance(argc, argv);

    // initialize parameter tree
    Parameters::init(argc, argv);

    //////////////////////////////////////////////////////////////////////
    // try to create a grid (from the given grid file or the input file)
    /////////////////////////////////////////////////////////////////////
    using BulkGrid = GetPropType<BulkTypeTag, Properties::Grid>;
    using FacetGrid = GetPropType<FacetTypeTag, Properties::Grid>;

    using GridManager = FacetCouplingGridManager<BulkGrid, FacetGrid>;
    GridManager gridManager;
    gridManager.init();
    gridManager.loadBalance();

    ////////////////////////////////////////////////////////////
    // run stationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid views
    const auto& bulkGridView = gridManager.template grid<0>().leafGridView();
    const auto& facetGridView = gridManager.template grid<1>().leafGridView();

    const bool writeVtk = getParam<bool>("Output.EnableVtk");
    if (writeVtk)
    {
        // write out the grids
        Dune::VTKWriter<typename BulkGrid::LeafGridView> bulkGridWriter(bulkGridView);
        Dune::VTKWriter<typename FacetGrid::LeafGridView> facetGridWriter(facetGridView);

        bulkGridWriter.write("bulkgrid");
        facetGridWriter.write("facetGrid");
    }

    // create the finite volume grid geometries
    using BulkFVGridGeometry = GetPropType<BulkTypeTag, Properties::GridGeometry>;
    using FacetFVGridGeometry = GetPropType<FacetTypeTag, Properties::GridGeometry>;
    auto bulkFvGridGeometry = std::make_shared<BulkFVGridGeometry>(bulkGridView);
    auto facetFvGridGeometry = std::make_shared<FacetFVGridGeometry>(facetGridView);
    updateBulkFVGridGeometry(*bulkFvGridGeometry, gridManager, facetGridView);
    facetFvGridGeometry->update();

    // get grid data for fractures
    auto bulkGridData = gridManager.getGridData()->template getSubDomainGridData<0>();
    auto facetGridData = gridManager.getGridData()->template getSubDomainGridData<1>();

    // the coupling mapper
    using TestTraits = ScenarioOneTestTraits<BulkTypeTag, FacetTypeTag>;
    auto couplingMapper = std::make_shared<typename TestTraits::CouplingMapper>();
    couplingMapper->update(*bulkFvGridGeometry, *facetFvGridGeometry, gridManager.getEmbeddings());

    // the coupling manager
    using CouplingManager = typename TestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the problems (boundary conditions)
    using BulkProblem = GetPropType<BulkTypeTag, Properties::Problem>;
    using FacetProblem = GetPropType<FacetTypeTag, Properties::Problem>;
    auto bulkSpatialParams = std::make_shared<typename BulkProblem::SpatialParams>(bulkFvGridGeometry, bulkGridData);
    auto bulkProblem = std::make_shared<BulkProblem>(bulkFvGridGeometry, bulkSpatialParams, couplingManager);
    auto facetSpatialParams = std::make_shared<typename FacetProblem::SpatialParams>(facetFvGridGeometry, facetGridData);
    auto facetProblem = std::make_shared<FacetProblem>(facetFvGridGeometry, facetSpatialParams, couplingManager);

    // the solution vector
    using MDTraits = typename TestTraits::MDTraits;
    using SolutionVector = typename MDTraits::SolutionVector;
    SolutionVector x, xOld;

    // apply initial solution
    static const auto bulkId = typename MDTraits::template SubDomain<0>::Index();
    static const auto facetId = typename MDTraits::template SubDomain<1>::Index();
    x[bulkId].resize(bulkFvGridGeometry->numDofs());
    x[facetId].resize(facetFvGridGeometry->numDofs());
    x = 0.0;

    // the problems must be available in cm before initializing the solution
    couplingManager->init(bulkProblem, facetProblem, couplingMapper, x);
    bulkProblem->setInitialPressure(x[bulkId]);
    facetProblem->setInitialPressure(x[facetId]);
    couplingManager->updateSolution(x);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkTypeTag, Properties::GridVariables>;
    using FacetGridVariables = GetPropType<FacetTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkFvGridGeometry);
    auto facetGridVariables = std::make_shared<FacetGridVariables>(facetProblem, facetFvGridGeometry);

    // intialize the vtk output module
    using BulkSolutionVector = std::decay_t<decltype(x[bulkId])>;
    using FacetSolutionVector = std::decay_t<decltype(x[facetId])>;
    using BulkVtkOutputModule = VtkOutputModule<BulkGridVariables, BulkSolutionVector>;
    using FacetVtkOutputModule = VtkOutputModule<FacetGridVariables, FacetSolutionVector>;

    const auto initSolBulkFileName = getParam<std::string>("InitialConditions.InitSolBulkFileName");
    const auto initSolFacetFileName = getParam<std::string>("InitialConditions.InitSolFacetFileName");
    if (getParam<bool>("InitialConditions.ComputeInitialSolution"))
    {
        // do a pressure solve with incompressible water to get an estimate of initial pressure
        using IncompTraits = ScenarioOneTestTraits<BulkIncompTypeTag, FacetIncompTypeTag>;
        pressureSolve<BulkIncompTypeTag,  FacetIncompTypeTag, IncompTraits>
                     (bulkFvGridGeometry, facetFvGridGeometry, gridManager,
                                                 /*dt*/1.0, /*tEnd*/3.0, x);

        // do a pressure solve with compressible water to get better estimate of initial pressure
        const auto equilLength = getParam<double>("TimeLoop.PressureEquilibrationPhaseLength");
        pressureSolve<BulkTypeTag, FacetTypeTag, TestTraits>
                     (bulkFvGridGeometry, facetFvGridGeometry, gridManager,
                                                 /*dt*/1.0, equilLength, x);

        // write initial pressure solution to disk
        std::cout << "\n\033[1;31mWriting computed intial solutions\033[0m\n";
        std::ofstream bulkInitSol(initSolBulkFileName, std::ios::out);
        std::ofstream facetInitSol(initSolFacetFileName, std::ios::out);

        for (const auto& priVars : x[bulkId]) bulkInitSol << priVars[0] << std::endl;
        for (const auto& priVars : x[facetId]) facetInitSol << priVars[0] << std::endl;
    }
    else
    {
        std::cout << "\n\033[1;31mReading in intial solutions\033[0m\n";
        std::ifstream bulkInitSol(initSolBulkFileName, std::ios::in);
        std::ifstream facetInitSol(initSolFacetFileName, std::ios::in);

        std::vector<double> initPressureBulk;
        std::vector<double> initPressureFacet;

        initPressureBulk.reserve(bulkFvGridGeometry->numDofs());
        initPressureFacet.reserve(facetFvGridGeometry->numDofs());

        double p;
        while (bulkInitSol >> p) initPressureBulk.push_back(p);
        while (facetInitSol >> p) initPressureFacet.push_back(p);

        auto updateSolutionVector = [] (auto& sol, const auto& initSol, const auto& gg)
        {
            using GG = std::decay_t<decltype(gg)>;
            for (const auto& element : elements(gg.gridView()))
            {
                if constexpr (GG::discMethod == DiscretizationMethod::box)
                {
                    static constexpr int dim = GG::GridView::dimension;
                    for (int i = 0; i < element.subEntities(dim); ++i)
                    {
                        const auto vIdx = gg.vertexMapper().subIndex(element, i, dim);
                        sol[vIdx][0] = initSol[vIdx];
                    }
                }
                else
                {
                    const auto eIdx = gg.elementMapper().index(element);
                    sol[eIdx][0] = initSol[eIdx];
                }
            }
        };

        updateSolutionVector(x[bulkId], initPressureBulk, *bulkFvGridGeometry);
        updateSolutionVector(x[facetId], initPressureFacet, *facetFvGridGeometry);
    }

    // Initialization run is over, consider injection and methane
    bulkProblem->setInitialSaturation(x[bulkId]);
    facetProblem->setInitialSaturation(x[facetId]);
    bulkProblem->setConsiderInjection(getParam<bool>("Problem.ConsiderInjection"));
    facetProblem->setConsiderInjection(getParam<bool>("Problem.ConsiderInjection"));
    bulkProblem->setConsiderMethane(true);
    facetProblem->setConsiderMethane(true);

    couplingManager->init(bulkProblem, facetProblem, couplingMapper, x);
    bulkGridVariables->init(x[bulkId]);
    facetGridVariables->init(x[facetId]);
    xOld = x;

    static constexpr bool bulkIsBox = BulkFVGridGeometry::discMethod == DiscretizationMethod::box;
    const auto bulkDataMode = bulkIsBox ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    BulkVtkOutputModule bulkVtkWriter(*bulkGridVariables, x[bulkId], bulkProblem->name(), "", bulkDataMode);
    FacetVtkOutputModule facetVtkWriter(*facetGridVariables, x[facetId], facetProblem->name());

    // Add model specific output fields
    using BulkIOFields = GetPropType<BulkTypeTag, Properties::IOFields>;
    using FacetIOFields = GetPropType<FacetTypeTag, Properties::IOFields>;
    BulkIOFields::initOutputModule(bulkVtkWriter);
    FacetIOFields::initOutputModule(facetVtkWriter);

    // add fault element marker
    std::vector<bool> isOnFault(facetGridView.size(0), false);
    for (const auto& e : elements(facetGridView))
        if (facetSpatialParams->isOnFault(e))
            isOnFault[facetFvGridGeometry->elementMapper().index(e)] = true;
    facetVtkWriter.addField(isOnFault, "isOnFault", FacetVtkOutputModule::FieldType::element);

    // Velocity output
    using BulkFluxVariables = GetPropType<BulkTypeTag, Properties::FluxVariables>;
    using FacetFluxVariables = GetPropType<FacetTypeTag, Properties::FluxVariables>;
    // PorousMediumFlowVelocityOutput<BulkGridVariables, BulkFluxVariables> bulkVelOutput(*bulkGridVariables);
    // PorousMediumFlowVelocityOutput<FacetGridVariables, FacetFluxVariables> facetVelOutput(*facetGridVariables);

    using Velocity = Dune::FieldVector<double, BulkGrid::dimensionworld>;
    std::vector<Velocity> bulkVelocity_w(bulkFvGridGeometry->gridView().size(0), Velocity(0.0));
    std::vector<Velocity> bulkVelocity_n(bulkFvGridGeometry->gridView().size(0), Velocity(0.0));
    std::vector<Velocity> facetVelocity_w(facetFvGridGeometry->gridView().size(0), Velocity(0.0));
    std::vector<Velocity> facetVelocity_n(facetFvGridGeometry->gridView().size(0), Velocity(0.0));

    bulkVtkWriter.addField(bulkVelocity_w, "v_w", BulkVtkOutputModule::FieldType::element);
    bulkVtkWriter.addField(bulkVelocity_n, "v_n", BulkVtkOutputModule::FieldType::element);
    facetVtkWriter.addField(facetVelocity_w, "v_w", FacetVtkOutputModule::FieldType::element);
    facetVtkWriter.addField(facetVelocity_n, "v_n", FacetVtkOutputModule::FieldType::element);

    // instantiate time loop
    using TimeLoop = CheckPointTimeLoop<double>;
    const auto injectionDuration = getParam<double>("BoundaryConditions.InjectionDuration");
    auto timeLoop = std::make_shared< TimeLoop >(/*startTime*/0.0,
                                                 getParam<double>("TimeLoop.DtInitial"),
                                                 getParam<double>("TimeLoop.TEnd"));
    timeLoop->setMaxTimeStepSize(getParam<double>("TimeLoop.MaxTimeStepSize"));
    timeLoop->setCheckPoint(injectionDuration);
    timeLoop->setCheckPoint(getParam<double>("TimeLoop.TEnd"));

    // the assembler
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(bulkProblem, facetProblem),
                                                  std::make_tuple(bulkFvGridGeometry, facetFvGridGeometry),
                                                  std::make_tuple(bulkGridVariables, facetGridVariables),
                                                  couplingManager, timeLoop, xOld);

    // the linear solver
    using LinearSolver = BlockDiagAMGBiCGSTABSolver;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    // (maybe) write initial solution TODO: implement compatible for box/tpfa
    auto computeVelocities = [&] (const auto& domainId,
    const auto& gg, const auto& gv, const auto& x,
    const auto& vo, auto& vel_w, auto& vel_n)
    {
        // for (const auto& e : elements(gg.gridView()))
        // {
            // couplingManager->bindCouplingContext(domainId, e, *assembler);
            // auto fvg = localView(gg);
            // auto evv = localView(gv.curGridVolVars());
            // auto efc = localView(gv.gridFluxVarsCache());
            //
            // fvg.bind(e);
            // evv.bind(e, fvg, x);
            // efc.bind(e, fvg, evv);
            //
            // vo.calculateVelocity(vel_w, e, fvg, evv, efc, 0);
            // vo.calculateVelocity(vel_n, e, fvg, evv, efc, 1);
        // }
    };

    if (writeVtk)
    {
        std::cout << "\n\033[1;32mWriting vtk output\033[0m\n";
        // computeVelocities(bulkId, *bulkFvGridGeometry, *bulkGridVariables, x[bulkId], bulkVelOutput, bulkVelocity_w, bulkVelocity_n);
        // computeVelocities(facetId, *facetFvGridGeometry, *facetGridVariables, x[facetId], facetVelOutput, facetVelocity_w, facetVelocity_n);
        bulkVtkWriter.write(0.0);
        facetVtkWriter.write(0.0);
    }

    // clear the .csv file before generating new data
    const auto csvFileName = getParam<std::string>("Output.CsvFile");
    {
        std::ofstream csvFile(csvFileName, std::ios::out);
    }

    // time loop
    timeLoop->start(); do
    {
        // set the time in the bulk problem
        bulkProblem->setTime(timeLoop->time()+timeLoop->timeStepSize());
        facetProblem->setTime(timeLoop->time()+timeLoop->timeStepSize());

        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // solve the non-linear system with time step control
        newtonSolver->solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        bulkGridVariables->advanceTimeStep();
        facetGridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        if (writeVtk)
        {
            std::cout << "\n\033[1;32mWriting vtk output\033[0m\n";
            // computeVelocities(bulkId, *bulkFvGridGeometry, *bulkGridVariables, x[bulkId], bulkVelOutput, bulkVelocity_w, bulkVelocity_n);
            // computeVelocities(facetId, *facetFvGridGeometry, *facetGridVariables, x[facetId], facetVelOutput, facetVelocity_w, facetVelocity_n);
            bulkVtkWriter.write(timeLoop->time());
            facetVtkWriter.write(timeLoop->time());
        }

        // (maybe deactivate injection)
        if (timeLoop->isCheckPoint())
        {
            using std::abs;
            if (abs(timeLoop->time() - injectionDuration) < 1e-6)
            {
                std::cout << "\n\034[1;32mDeactivating injection in the problems\033[0m\n";
                bulkProblem->setConsiderInjection(false);
                facetProblem->setConsiderInjection(false);
            }
        }

        // write csv data (in first time step, write header)
        std::ofstream csvFile(csvFileName, std::ios::app);

        auto bindBulkContext = [&] (const auto& e) { couplingManager->bindCouplingContext(bulkId, e, *assembler); };
        auto bindFacetContext = [&] (const auto& e) { couplingManager->bindCouplingContext(facetId, e, *assembler); };

        const auto bulkData = bulkProblem->getPostProcessingData(bindBulkContext, *bulkGridVariables, x[bulkId]);
        const auto facetData = facetProblem->getPostProcessingData(bindFacetContext, *facetGridVariables, x[facetId]);

        if (timeLoop->timeStepIndex() == 1)
        {
            csvFile << "time,";
            for (const auto& data : bulkData) csvFile << data.first << ",";
            for (int i = 0; i < facetData.size()-1; ++i) csvFile << facetData[i].first << ",";
            csvFile << facetData.back().first << "\n";
        }
        csvFile << timeLoop->time() << ",";
        for (const auto& data : bulkData) csvFile << data.second << ",";
        for (int i = 0; i < facetData.size()-1; ++i) csvFile << facetData[i].second << ",";
        csvFile << facetData.back().second << "\n";

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the Newton solver
        timeLoop->setTimeStepSize(newtonSolver->suggestTimeStepSize(timeLoop->timeStepSize()));

        // print the time in a more human-understandable unit to terminal
        std::cout << "\n\033[1;31mCurrent time in hours: " << timeLoop->time()/3600.0 << "\033[0m\n";

    } while (!timeLoop->finished());

    // output some statistics
    newtonSolver->report();
    timeLoop->finalize();

    return 0;

}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
