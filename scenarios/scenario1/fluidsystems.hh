// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The fluid systems used in scenario 1.
 */
#ifndef DUMUX_DELLOCCA2020_SCENARIO1_FLUIDSYSTEMS_HH
#define DUMUX_DELLOCCA2020_SCENARIO1_FLUIDSYSTEMS_HH

#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/ch4.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>

namespace Dumux::ScenarioOne {

using namespace Dumux::FluidSystems;
using namespace Dumux::Components;

template<class Scalar>
using FluidSystemIncompressibleH2O = TwoPImmiscible< Scalar,
                                                     OnePLiquid<Scalar, SimpleH2O<Scalar>>,
                                                     OnePGas<Scalar, CH4<Scalar>> >;

template<class Scalar>
using FluidSystemCompressibleH2O = TwoPImmiscible< Scalar,
                                                   OnePLiquid<Scalar, TabulatedComponent<H2O<Scalar>>>,
                                                   OnePGas<Scalar, CH4<Scalar>> >;

} // end namespace Dumux::ScenarioOne

#endif
