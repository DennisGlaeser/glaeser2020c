// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The properties for the bulk domain in scenario1.
 */
#ifndef DUMUX_DELLOCCA2020_SCENARIO1_BULK_FLOW_PROPERTIES_HH
#define DUMUX_DELLOCCA2020_SCENARIO1_BULK_FLOW_PROPERTIES_HH

#include <dune/alugrid/grid.hh>

#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/ch4.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>

#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>
#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/porousmediumflow/2p/model.hh>

#include "fluidsystems.hh"
#include "problem_bulk.hh"
#include "spatialparams_bulk.hh"

namespace Dumux::Properties {

// create the type tag node
namespace TTag {
struct TwoPBulk { using InheritsFrom = std::tuple<TwoP>; };

struct TwoPBulkCompressibleTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, TwoPBulk>; };
struct TwoPBulkIncompressibleTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, TwoPBulk>; };

struct TwoPBulkCompressibleMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, TwoPBulk>; };
struct TwoPBulkIncompressibleMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, TwoPBulk>; };


struct TwoPBulkCompressibleBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, TwoPBulk>; };
struct TwoPBulkIncompressibleBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, TwoPBulk>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPBulk> { using type = Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPBulk> { using type = TwoPBulkProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPBulk>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

public:
    using type = TwoPBulkSpatialParams<GridGeometry, Scalar>;
};

// pressure-independent permeabilities
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::TwoPBulk>
{ static constexpr bool value = false; };

// the fluid systems
using namespace ScenarioOne;

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPBulkCompressibleTpfa>
{ using type = FluidSystemCompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPBulkIncompressibleTpfa>
{ using type = FluidSystemIncompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPBulkCompressibleMpfa>
{ using type = FluidSystemCompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPBulkIncompressibleMpfa>
{ using type = FluidSystemIncompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPBulkCompressibleBox>
{ using type = FluidSystemCompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPBulkIncompressibleBox>
{ using type = FluidSystemIncompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

} // end namespace Dumux::Properties

#endif
