// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial params for the facet domain in scenario 1
 */
#ifndef DUMUX_DELLOCCA2020_SCENARIO1_FACET_SPATIAL_PARAMS_HH
#define DUMUX_DELLOCCA2020_SCENARIO1_FACET_SPATIAL_PARAMS_HH

#include <dumux/io/grid/griddata.hh>

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux {

template< class FVGridGeometry, class Scalar >
class TwoPFacetSpatialParams
: public FVSpatialParams< FVGridGeometry, Scalar, TwoPFacetSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = TwoPFacetSpatialParams< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParams< FVGridGeometry, Scalar, ThisType >;

    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using GridView = typename FVGridGeometry::GridView;
    using Grid = typename GridView::Grid;
    static constexpr int dim = GridView::dimension;

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    // use a linear  material law
    using EffectiveLaw = LinearMaterial<Scalar>;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! export the material law and parameters used
    using MaterialLaw = EffToAbsLaw< EffectiveLaw >;
    using MaterialLawParams = typename MaterialLaw::Params;

    //! we assume the fault to have the marker 1 (must be matched by the mesh file)
    static constexpr int faultMarker = 1;

    //! the constructor
    TwoPFacetSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                           std::shared_ptr<const Dumux::GridData<Grid>> gridData)
    : ParentType(fvGridGeometry)
    , gridDataPtr_(gridData)
    , fracPorosity_(getParam<Scalar>("SpatialParams.Fractures.Porosity"))
    , faultPorosity_(getParam<Scalar>("SpatialParams.Fault.Porosity"))
    , fracPermeability_(getParam<Scalar>("SpatialParams.Fractures.Permeability"))
    , faultPermeability_(getParam<Scalar>("SpatialParams.Fault.Permeability"))
    {
        fracMaterialLawParams_.setEntryPc(getParam<Scalar>("SpatialParams.Fractures.EntryPc"));
        fracMaterialLawParams_.setMaxPc(getParam<Scalar>("SpatialParams.Fractures.MaxPc"));

        faultMaterialLawParams_.setEntryPc(getParam<Scalar>("SpatialParams.Fault.EntryPc"));
        faultMaterialLawParams_.setMaxPc(getParam<Scalar>("SpatialParams.Fault.MaxPc"));

        vertexOnFault_.resize(this->gridGeometry().gridView().size(dim), false);
        for (const auto& e : elements(this->gridGeometry().gridView()))
            if (isOnFault(e))
                for (unsigned int i = 0; i < e.subEntities(dim); ++i)
                    vertexOnFault_[this->gridGeometry().vertexMapper().index(e.template subEntity<dim>(i))] = true;
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    { return isOnFault(element) ? faultPermeability_ : fracPermeability_; }

    //! Return the porosity
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    { return isOnFault(element) ? faultPorosity_ : fracPorosity_; }

    //! Return the material law parameters
    template<class ElementSolution>
    const MaterialLawParams& materialLawParams(const Element& element,
                                               const SubControlVolume& scv,
                                               const ElementSolution& elemSol) const
    { return isOnFault(element) ? faultMaterialLawParams_ : fracMaterialLawParams_; }

    //! Water is the wetting phase
    template< class FluidSystem >
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        // water is the first phase in the fluid system
        return FluidSystem::phase0Idx;
    }

    //! returns the domain marker for an element
    int getElementDomainMarker(const Element& element) const
    { return gridDataPtr_->getElementDomainMarker(element); }

    //! returns true if an element lies on the fault
    int isOnFault(const Element& element) const
    { return getElementDomainMarker(element) == faultMarker; }

    //! returns true if an element lies on the fault
    int isOnFault(const Vertex& v) const
    { return vertexOnFault_[this->gridGeometry().vertexMapper().index(v)]; }

    //! return the material law parameters of the fractures
    const MaterialLawParams& fractureMaterialLawParams() const
    { return fracMaterialLawParams_; }

private:
    //! pointer to the grid data (contains domain markers)
    std::shared_ptr<const Dumux::GridData<Grid>> gridDataPtr_;

    Scalar fracPorosity_;
    Scalar faultPorosity_;

    PermeabilityType fracPermeability_;
    PermeabilityType faultPermeability_;

    MaterialLawParams fracMaterialLawParams_;
    MaterialLawParams faultMaterialLawParams_;

    std::vector<bool> vertexOnFault_;
};

} // end namespace Dumux

#endif
