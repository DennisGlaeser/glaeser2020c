# print welcome message
print("\n\n"
      "#################################################################\n"
      "## Generator for the domain and fracture network of scenario 1 ##\n"
      "#################################################################\n"
      "\n\n")

import sys
import time
import argparse

if sys.version_info[0] < 3:
    sys.exit("Python3 required\n")

start = time.time()
parser = argparse.ArgumentParser(description='Domain and fracture network generation for scenario1')
parser.add_argument('-r',    '--radius',               default=80.0,  type=float, help='radius (in meter) of the spherical domain')
parser.add_argument('-e',    '--extent',               default=40,    type=float, help='horizontal extent up to which fractures are admitted')
parser.add_argument('-s',    '--spacing',              default=1.0,   type=float, help='fracture spacing (in meter)')
parser.add_argument('-l',    '--length',               default=15.0,  type=float, help='characteristic length of the fractures')
parser.add_argument('-z',    '--height',               default=8.0,   type=float, help='characteristic height of the fractures')
parser.add_argument('-w',    '--wellradius',           default=0.5,   type=float, help='radius of the well, placed at the center of the domain')
parser.add_argument('-i',    '--injectionlength',      default=5.0,   type=float, help='length of the part of the well through which injection occurs')
parser.add_argument('-t',    '--sourcethickness',      default=50.0,  type=float, help='height of the shale formation, in whose center the well is placed')
parser.add_argument('-ml',   '--mainfraclength',       default=20.0,  type=float, help='characteristic length of the first (main) fracture')
parser.add_argument('-ma',   '--mainfracaspectratio',  default=0.5,   type=float, help='aspect ratio (horizontal/vertical extent) of the first (main) fracture')
parser.add_argument('-dxf',  '--meshsizeatfracs',      default=1.0,   type=float, help='mesh size to be used on fractures')
parser.add_argument('-dxm',  '--meshsizeinmatrix',     default=10.0,  type=float, help='mesh size to be used in the rest of the medium')
parser.add_argument('-v',    '--verbosity',            action='store_true'      , help='turn on/off output on rejected entities during network generation')
parser.add_argument('-nc',   '--numcycles',            default=4,     type=int,   help='number of entity generation cycles')
parser.add_argument('-ne',   '--numcycleentities',     default=4,     type=int,   help='number of entities to be generated per cycles')
parser.add_argument('-ct',   '--caprockthickness',     default=50.0,  type=int,   help='thickness of the caprock above the source formation')
parser.add_argument('-fcte', '--fraccaprockextension', default=5.0,   type=int,   help='defines how far the fractures reach into the caprock')
args = vars(parser.parse_args())

if args['extent'] > args['radius']:
    sys.exit('radius must be larger than extent!')


######################
# 1. Define the domain
from frackit.geometry import Point, Vector, Direction, Sphere, Box, Disk, Ellipse_3, Cylinder
from frackit.occutilities import intersect, cut

tolerance = args['radius']*1e-7
a = args['radius']*1.25 # use 25% larger box to guarantee correct intersection
b = args['sourcethickness']

# domain is a sphere around the well restricted to the source formation
domain = intersect( Sphere(Point(0.0, 0.0, 0.0), args['radius']),
                    Box(-a, -a, -b/2.0, a, a, b/2.0),
                    tolerance )

# the fractures extend up to the caprock as well
domainAndCaprock = intersect( Sphere(Point(0.0, 0.0, 0.0), args['radius']),
                              Box(-a, -a, -b/2.0, a, a, b/2.0 + args['caprockthickness']),
                              tolerance )

# cut out injection well
wellBottomCenter = Point(0.0, -args['injectionlength']/2.0, 0.0)
wellBottom = Disk(Ellipse_3(wellBottomCenter,
                            Direction(Vector(-1.0, 0.0, 0.0)),
                            Direction(Vector( 0.0, 0.0, 1.0)),
                            args['wellradius'],
                            args['wellradius']))
well = Cylinder(wellBottom, args['injectionlength'])

domain = cut(domain, well, tolerance)
domainAndCaprock = cut(domainAndCaprock, well, tolerance)

# the TopoDS_Solid representation of the domains
from frackit.occutilities import getSolids
domainSolid = getSolids(domain)
domainAndCaprockSolid = getSolids(domainAndCaprock)
if len(domainSolid) != 1: sys.exit("Error while attempting to get solid representation of domain")
if len(domainAndCaprockSolid) != 1: sys.exit("Error while attempting to get solid representation of domain+caprock")
domainSolid = domainSolid[0]
domainAndCaprockSolid = domainAndCaprockSolid[0]

# cylinder representing the regions in which fractures are admitted
from frackit.geometry import Cylinder
fracturedRegion = Cylinder(args['extent'], b + args['fraccaprockextension'], -b/2.0)


################################################
# 2. Define the samplers for fracture geometries
import random
def normalSampler(mean, stdDev):
    def sample(): return random.gauss(mean, stdDev)
    return sample

# makes a disk sampler instance for a given id
from frackit.common import toRadians
from frackit.sampling import DiskSampler
def makeSampler(id, pointSampler):
    if id == 0: return DiskSampler(pointSampler,                                       # sampler for disk center points
                                   normalSampler(args['length'], args['length']/20.0), # major axis length: mean value & standard deviation
                                   normalSampler(args['height'], args['height']/20.0), # minor axis length: mean value & standard deviation
                                   normalSampler(toRadians(90.0), toRadians(90/15.0)), # rotation around x-axis: mean value & standard deviation
                                   normalSampler(toRadians(0.0),  toRadians(90/5.0)),  # rotation around y-axis: mean value & standard deviation
                                   normalSampler(toRadians(90.0), toRadians(90/15.0))) # rotation around z-axis: mean value & standard deviation
    elif id == 1: return DiskSampler(pointSampler,                                       # sampler for disk center points
                                     normalSampler(args['length'], args['length']/20.0), # major axis length: mean value & standard deviation
                                     normalSampler(args['height'], args['height']/20.0), # minor axis length: mean value & standard deviation
                                     normalSampler(toRadians(90.0), toRadians(90/15.0)), # rotation around x-axis: mean value & standard deviation
                                     normalSampler(toRadians(0.0),  toRadians(90/5.0)),  # rotation around y-axis: mean value & standard deviation
                                     normalSampler(toRadians(0.0),  toRadians(90/15.0))) # rotation around z-axis: mean value & standard deviation
    else: sys.exit("Invalid id provided")


##########################
# 3. Define the fault zone
from math import sin, cos
from frackit.common import rotate
faultStrikeAngle = toRadians(15.0)
faultDipAngle = toRadians(15.0)
faultXOffset = 0.0

faultMajAx = Direction(Vector(cos(faultStrikeAngle), sin(faultStrikeAngle), 0.0))
faultMinAx = Vector(0.0, 0.0, 1.0)
rotate(faultMinAx, faultMajAx, faultStrikeAngle)
faultMinAx = Direction(faultMinAx)

c1 = Point(0.0, 0.0, 0.0) - faultMajAx*2000.0
c2 = Point(0.0, 0.0, 0.0) + faultMajAx*2000.0
c3 = c1 + faultMinAx*2000.0
c4 = c2 + faultMinAx*2000.0

from frackit.geometry import Quadrilateral_3
fault = Quadrilateral_3(c1, c2, c3, c4)

# we confine the fault zone to the domain used in scenario 2 (with caprock)
from frackit.occutilities import getFaces
domainS2 = Box(-500, -500, b/2.0, 500, 500, 1000)
fault = intersect(fault, domainS2, tolerance)
fault = getFaces(fault)
if len(fault) != 1: sys.exit("Could not construct the fault")
fault = fault[0]


###################################
# 4. Generate first (main) fracture
mainFracCenter = Point(0.0, 0.0, 0.0)
mainFracMajAxis = Direction(Vector(0.0, 0.0, 1.0))
mainFracMinAxis = Direction(Vector(-1.0, 0.0, 0.0))
mainFracMaxAxisLength = args['mainfraclength']
mainFracMinAxisLength = args['mainfraclength']*args['mainfracaspectratio']
mainFracture = Disk(Ellipse_3(mainFracCenter,
                              mainFracMajAxis, mainFracMinAxis,
                              mainFracMaxAxisLength, mainFracMinAxisLength))
mainFracture = intersect(mainFracture, domainAndCaprock, tolerance)
mainFracture = getFaces(mainFracture)
if len(mainFracture) != 1:
    sys.exit("Error during generation of main fracture\n")
mainFracture = mainFracture[0]


###################################################################
# 5. Define constraints that should be fulfilled among the entities
from frackit.entitynetwork import EntityNetworkConstraints
constraints = EntityNetworkConstraints()
constraints.setMinDistance(args['spacing'])
constraints.setMinIntersectionDistance(2.5)
constraints.setMinIntersectionMagnitude(2.5)
constraints.setMinIntersectingAngle(toRadians(20.0))


#######################
# 6. Network generation
from frackit.common import Id
from frackit.sampling import SamplingStatus

# We generate the network in cycles. In each cycle, we
# create entities of a specific orientation, and we
# alternate between orientation 1 and 0. Moreover, we
# define a number of entities per cycle, and each one
# should intersect at least one entity of the previous one.
samplingCycles = [[0, SamplingStatus()]]
samplingCycles[-1][1].setTargetCount(Id(0), args['numcycleentities'])
for i in range(1, args['numcycles']):
    id = 1 if samplingCycles[i-1][0] == 0 else 0
    samplingCycles.append([id, SamplingStatus()])
    samplingCycles[-1][1].setTargetCount(Id(id), args['numcycleentities'])

curFractureSet = []
prevFractureSet = [mainFracture]
fractures = [mainFracture]

from frackit.occutilities import getShape, getEdges, fragment, getBoundingBox
from frackit.sampling import makeUniformPointSampler
from frackit.geometry import Box
for cycleCount, [id, status] in enumerate(samplingCycles):

    print("\n\033[1;31mStarting sampling cycle {:d} of {:d}\033[0m".format(cycleCount+1, len(samplingCycles)))
    print("\033[1;30mLooking for {:d} fractures that intersect previously inserted ones\033[0m\n".format(args['numcycleentities']))

    while not status.finished(Id(id)):

        # for the first two cycles, sample a position close to a random
        # pick of the previously created entities
        if cycleCount < 2:
            prevIndices = [i for i in range(0, len(prevFractureSet))]
            bBox = getBoundingBox(prevFractureSet[random.choice(prevIndices)])

            # don't even bother to sample if the box center is outside the fractured radius
            if not fracturedRegion.contains(bBox.center()):
                if args['verbosity']: print("Reject: sample box is outside the fractured region")
                continue

            dx = bBox.xMax() - bBox.xMin()
            dy = bBox.yMax() - bBox.yMin()
            dz = bBox.zMax() - bBox.zMin()

            xMin = bBox.xMin() - dx/2.0; xMax = bBox.xMax() + dx/2.0;
            yMin = bBox.yMin() - dy/2.0; yMax = bBox.yMax() + dy/2.0;
            zMin = bBox.zMin() - dz/2.0; zMax = bBox.zMax() + dz/2.0;

            if cycleCount == 0:
                sampleBox1 = Box(xMin, yMin, zMin + 0.25*dz, xMax, yMax, zMax)
                sampleBox2 = sampleBox1
            elif id == 1:
                sampleBox1 = Box(xMin, yMin, zMin, xMax, yMin + 0.75*dy, zMax)
                sampleBox2 = Box(xMin, yMax - 0.75*dy, zMin, xMax, yMax, zMax)
            else:
                sampleBox1 = Box(xMin, yMin, zMin, xMin + 0.75*dx, yMax, zMax)
                sampleBox2 = Box(xMax - 0.75*dx, yMin, zMin, xMax, yMax, zMax)

            samplers = [makeSampler(id, makeUniformPointSampler(sampleBox1)),
                        makeSampler(id, makeUniformPointSampler(sampleBox2))]

        # otherwise sample within the fractured region
        else:
            samplers = [makeSampler(id, makeUniformPointSampler(fracturedRegion)),
                        makeSampler(id, makeUniformPointSampler(fracturedRegion))]

        # sample entity and confine it
        geom = samplers[random.choice([0, 1])]()
        containedGeom = intersect(geom, domainAndCaprock, tolerance)
        containedGeom = getFaces(containedGeom)

        # skip entites that do not lead to a single face
        if len(containedGeom) != 1:
            if args['verbosity']: print("Reject: entity is not contained in source")
            status.increaseRejectedCounter()
            continue

        containedGeom = containedGeom[0]

        # returns true if entity intersect prevFracture
        def doesIntersect(geom, prevFrac):
            numEdgesBefore = len(getEdges(geom)) + len(getEdges(prevFrac))
            result = fragment([geom, prevFrac], tolerance)
            return len(getEdges(result)) > numEdgesBefore

        # Reject non-intersecting fractures
        if not any(doesIntersect(containedGeom, f) for f in prevFractureSet):
            if args['verbosity']: print("Reject: entity does not intersect network")
            status.increaseRejectedCounter()
            continue

        # We want to avoid small fragments
        from frackit.magnitude import computeMagnitude
        if computeMagnitude(containedGeom) < 20.0:
            if args['verbosity']: print("Reject: entity fragment too small")
            status.increaseRejectedCounter()
            continue

        # enforce constraints w.r.t. to the other entities
        if not constraints.evaluate(fractures, containedGeom):
            if args['verbosity']: print("Reject: constraints violated")
            status.increaseRejectedCounter()
            continue

        # enforce constraints w.r.t. to the other entities
        if not constraints.evaluate(curFractureSet, containedGeom):
            if args['verbosity']: print("Reject: constraints violated")
            status.increaseRejectedCounter()
            continue

        # enforce constraints w.r.t. to the fault
        if not constraints.evaluate(fault, containedGeom):
            status.increaseRejectedCounter()
            continue

        # the geometry is admissible
        curFractureSet.append(containedGeom)
        status.increaseCounter(Id(id))
        status.print()

    # prepare next cycle
    fractures.extend(curFractureSet)
    prevFractureSet = curFractureSet
    curFractureSet = []

print("\n -- Entity creation finished")


##########################
# 6. Write network to disk
from frackit.entitynetwork import ContainedEntityNetworkBuilder
builder = ContainedEntityNetworkBuilder()

# we use a higher tolerance here as the overall model (with fault) is very large
builder.setEpsilon(1e-2)
builder.setConfineToSubDomainUnion(False)

# choose non-confining subdomain, fractures are confined already
# NOTE: the mesh size at the fault & injection well have has to
#       be adjusted manually afterwards
builder.addSubDomain(well, Id(0))
builder.addSubDomain(domainSolid, Id(1))
builder.addSubDomainEntity(fault, Id(1))
builder.addSubDomainEntities(fractures, Id(1))

from frackit.io import GmshWriter
fileName = "scenario1"
gmshWriter = GmshWriter(builder.build());
gmshWriter.write("scenario1", args['meshsizeatfracs'], args['meshsizeinmatrix'])
print(" -- Geometry file written")

stop = time.time()
print(" -- Overall run time was {:.2f} seconds".format(stop-start))
