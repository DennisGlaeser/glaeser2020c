// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the facet domain in scenario 1
 */
#ifndef DUMUX_DELLOCCA2020_SCENARIO1_FACET_FLOW_PROBLEM_HH
#define DUMUX_DELLOCCA2020_SCENARIO1_FACET_FLOW_PROBLEM_HH

#include <dumux/discretization/cellcentered/tpfa/computetransmissibility.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/discretization/elementsolution.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/common/boundarytypes.hh>

namespace Dumux {

/*!
 * \brief The problem for the facet domain in the
 *        two-phase facet coupling test
 */
template<class TypeTag>
class TwoPFacetProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using BoundaryTypes = Dumux::BoundaryTypes<ModelTraits::numEq()>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

    using GridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;
    static constexpr int dim = GridView::dimension;

    // post-processing data
    using StringDataPair = std::pair<std::string, Scalar>;
    using StringDataPairVector = std::vector<StringDataPair>;

    // some indices for convenience
    using Indices = typename ModelTraits::Indices;
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx,

        // phase indices
        H2OPhaseIdx = FluidSystem::comp0Idx,
        CH4PhaseIdx = FluidSystem::comp1Idx,

        // first phase conti equation
        contiH2OEqIdx = Indices::conti0EqIdx + H2OPhaseIdx,
        contiCH4EqIdx = Indices::conti0EqIdx + CH4PhaseIdx
    };

public:
    TwoPFacetProblem(std::shared_ptr<const GridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     std::shared_ptr<CouplingManager> cm)
    : ParentType(fvGridGeometry, spatialParams)
    , couplingManagerPtr_(cm)
    , fracAperture_(getParam<Scalar>("SpatialParams.Fractures.Aperture"))
    , faultAperture_(getParam<Scalar>("SpatialParams.Fault.Aperture"))
    , aquiferThickness_(getParam<Scalar>("Problem.AquiferThickness"))
    , aquiferPermeability_(getParam<Scalar>("Problem.AquiferPermeability"))
    , injectionPressure_(getParam<Scalar>("BoundaryConditions.InjectionPressure"))
    , injectionLength_(getParam<Scalar>("Problem.InjectionLength"))
    , wellRadius_(getParam<Scalar>("Problem.WellRadius"))
    , time_(0.0)
    , considerMethane_(true)
    , considerInjection_(false)
    , useZeroInitialCH4Saturation_(getParam<bool>("InitialConditions.UseZeroMethaneSaturationFractures"))
    {
        // initialize the tabulation
        // of water properties. Use the default p/T ranges
        Components::TabulatedComponent< Dumux::Components::H2O<Scalar> >::init(273.15, 293.15, 2,
                                                                               9e4, 1e8, 1000);

        // set problem name
        this->setName(getParam<std::string>("Problem.Facet.Name"));
    }

    //! Specifies the type of boundary condition at a given position
    template<class SubControlEntity>
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlEntity& sce) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        GlobalPosition globalPos;
        if constexpr (isBox) globalPos = sce.dofPosition();
        else globalPos = sce.ipGlobal();

        if (isOnTopBoundary_(globalPos))
            values.setAllDirichlet();

        // for box, the injection is incorporated via a Dirichlet BC
        if (isBox && considerInjection_ && isOnWell_(globalPos))
            values.setDirichlet(contiH2OEqIdx);

        return values;
    }

    //! Evaluate the source term at a given position
    template<class ElementVolumeVariables>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

    //! evaluates the Dirichlet boundary condition for a given position (tpfa implementation)
    PrimaryVariables dirichlet(const Element& element,
                               const SubControlVolumeFace& scvf) const
    {
        if (isOnTopBoundary_(scvf.ipGlobal()))
            return PrimaryVariables({initialPressureAtPos(scvf.ipGlobal()), 0.0});
        DUNE_THROW(Dune::InvalidStateException, "Dirichlet BCs should only occur at top boundary");
    }

    //! evaluates the Dirichlet boundary condition for a given position (box implementation)
    PrimaryVariables dirichlet(const Element& element,
                               const SubControlVolume& scv) const
    {
        // saturation should not be used
        if (isOnWell_(scv.dofPosition()))
            return PrimaryVariables({injectionPressure_, 1.0});
        else if (isOnTopBoundary_(scv.dofPosition()))
            return PrimaryVariables({initialPressureAtPos(scv.dofPosition()), 0.0});
        DUNE_THROW(Dune::InvalidStateException, "Unexpected Dirichlet boundary segment");
    }

    //! Evaluate the Neumann boundary conditions
    template<class ElementVolumeVariables, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    { return neumann(element, fvGeometry, elemVolVars, scvf); }

    //! Evaluate the Neumann boundary conditions
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector flux(0.0);

        // For TPFA, we incorporate the injection weakly
        if constexpr (!isBox)
        {
            if (isOnWell_(scvf.ipGlobal()) && considerInjection_)
            {
                const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                const auto& insideVolVars = elemVolVars[insideScv];
                const auto& insideK = insideVolVars.permeability();
                const auto extrusion = insideVolVars.extrusionFactor();
                const auto t = -1.0/extrusion
                                   *computeTpfaTransmissibility(scvf, insideScv,
                                                                insideK, extrusion);

                const Scalar pw = insideVolVars.pressure(FluidSystem::phase0Idx);
                const Scalar pInj = injectionPressure_;

                const auto& n = scvf.unitOuterNormal();
                const auto& g = this->spatialParams().gravity(scvf.ipGlobal());
                const auto& rhoW = insideVolVars.density(FluidSystem::phase0Idx);
                const auto& muW = insideVolVars.viscosity(FluidSystem::phase0Idx);
                const auto rhoKng = n*g*insideK*rhoW;

                flux[contiH2OEqIdx] = t*(pInj - pw) + rhoKng;
                flux[contiH2OEqIdx] *= rhoW/muW; // TODO: mobility??
            }
        }

        return flux;
    }

    //! Set the aperture as extrusion factor.
    template<class ElemSol>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElemSol& elemSol) const
    {
        return this->spatialParams().isOnFault(element) ? faultAperture_
                                                        : fracAperture_;
    }

    //! evaluate the initial conditions
    template<class Entity>
    PrimaryVariables initial(const Entity& entity) const
    {
        const auto globalPos = entity.geometry().center();

        PrimaryVariables values;
        values[pressureIdx] = initialPressureAtPos(globalPos);
        if (considerMethane_ && !useZeroInitialCH4Saturation_ && !this->spatialParams().isOnFault(entity))
        {
            static const Scalar initSCH4 = computeInitialMethaneSaturation_();
            values[saturationIdx] = initSCH4;
        }
        else
            values[saturationIdx] = 0.0;

        return values;
    }

    //! returns the initial pressure at a given position
    Scalar initialPressureAtPos(const GlobalPosition& globalPos) const
    {
        // domain height is 2000m
        static const Scalar height = this->gridGeometry().bBoxMax()[2] + aquiferThickness_;

        // use density at atmospheric conditions for initial pressure distribution
        using FluidState = GetPropType<TypeTag, Properties::FluidState>;
        FluidState fs;
        fs.setPressure(FluidSystem::phase0Idx, 1e5);
        fs.setTemperature(FluidSystem::phase0Idx, temperature());

        const auto rho = FluidSystem::density(fs, FluidSystem::phase0Idx);
        const auto& g = this->spatialParams().gravity(globalPos);
        return 1.0e5 - (height - globalPos[2])*rho*g[2];
    }

    //! Set if methane should be considered/neglected
    void setConsiderMethane(bool value)
    { considerMethane_ = value; }

    //! update the current time
    void setTime(Scalar t)
    { time_ = t; }

    //! Set if the injection should be considered/neglected
    void setConsiderInjection(bool value)
    { considerInjection_ = value; }

    //! set the initial saturation distribution
    void setInitialSaturation(SolutionVector& x)
    {
        std::cout << "\033[1;33mSetting initial facet methane saturation\033[0m\n";

        if (!useZeroInitialCH4Saturation_ && considerMethane_)
        {
            static const Scalar initSCH4 = computeInitialMethaneSaturation_();
            for (const auto& e : elements(this->gridGeometry().gridView()))
            {
                if (this->spatialParams().isOnFault(e))
                    continue;

                if constexpr (!isBox)
                {
                    const auto eIdx = this->gridGeometry().elementMapper().index(e);
                    x[eIdx][saturationIdx] = initSCH4;
                }
                else
                {
                    for (unsigned int i = 0; i < e.subEntities(dim); ++i)
                    {
                        const auto& v = e.template subEntity<dim>(i);
                        const auto vIdx = this->gridGeometry().vertexMapper().subIndex(e, i, dim);
                        if (!this->spatialParams().isOnFault(v))
                            x[vIdx][saturationIdx] = initSCH4;
                    }
                }
            }
        }
    }

    //! set the initial pressure distribution
    void setInitialPressure(SolutionVector& x)
    {
        std::cout << "\033[1;33mSetting initial water pressure distribution\033[0m\n";

        for (const auto& e : elements(this->gridGeometry().gridView()))
        {
            if constexpr (!isBox)
            {
                const auto eIdx = this->gridGeometry().elementMapper().index(e);
                x[eIdx][pressureIdx] = initialPressureAtPos(e.geometry().center());
            }
            else
            {
                for (unsigned int i = 0; i < e.subEntities(dim); ++i)
                {
                    const auto& v = e.template subEntity<dim>(i);
                    const auto vIdx = this->gridGeometry().vertexMapper().subIndex(e, i, dim);
                    x[vIdx][pressureIdx] = initialPressureAtPos(v.geometry().center());
                }
            }
        }
    }

    //! Computes post-processing data for .csv output
    template<class BindContextFunc, class GridVariables, class Solution>
    std::vector<std::pair<std::string, Scalar>> getPostProcessingData(BindContextFunc&& bindContext,
                                                                      const GridVariables& gridVars,
                                                                      const Solution& x) const
    {
        using DataPair = std::pair<std::string, Scalar>;
        using Result = std::vector<DataPair>;

        Result result;
        result.emplace_back( std::make_pair("massCH4Fractures", 0.0) );
        result.emplace_back( std::make_pair("massCH4Fault", 0.0) );
        result.emplace_back( std::make_pair("transferFluxH2O", 0.0) );
        result.emplace_back( std::make_pair("transferFluxCH4", 0.0) );

        for (const auto& e : elements(this->gridGeometry().gridView()))
        {
            bindContext(e);
            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());

            fvGeometry.bind(e);
            elemVolVars.bind(e, fvGeometry, x);

            const bool isOnFault = this->spatialParams().isOnFault(e);
            const unsigned int massIdx = isOnFault ? 1 : 0;
            for (const auto& scv : scvs(fvGeometry))
            {
                result[massIdx].second += elemVolVars[scv].saturation(CH4PhaseIdx)
                                          *elemVolVars[scv].density(CH4PhaseIdx)
                                          *elemVolVars[scv].porosity()
                                          *elemVolVars[scv].extrusionFactor()
                                          *scv.volume();

                if (!isOnFault)
                {
                    const auto s = source(e, fvGeometry, elemVolVars, scv);
                    result[2].second += s[contiH2OEqIdx]
                                        *elemVolVars[scv].extrusionFactor()
                                        *scv.volume();
                    result[3].second += s[contiCH4EqIdx]
                                        *elemVolVars[scv].extrusionFactor()
                                        *scv.volume();
                }
            }
        }

        return result;
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

private:
    //! Compute initial methane saturation as function of the initial bulk source saturation
    Scalar computeInitialMethaneSaturation_() const
    {
        constexpr auto bulkId = Dune::index_constant<0>();
        const auto& bulkProblem = couplingManager().problem(bulkId);
        const auto& bulkSpatialParams = bulkProblem.spatialParams();

        using BulkSpatialParams = std::decay_t<decltype(bulkSpatialParams)>;
        using BulkMaterialLaw = typename BulkSpatialParams::MaterialLaw;

        static const Scalar initSourceSat = getParam<Scalar>("InitialConditions.InitialMethaneSaturationSource");
        static const auto pc = BulkMaterialLaw::pc(bulkSpatialParams.materialLawParams(), initSourceSat);

        // capillary pressures in the source might be higher than in the fractures
        // for all values of sw -> in that case we might get negative saturations
        // Therefore, use std::clamp to restrict to 0 <= sw <= 1.0
        using SpatialParams = typename ParentType::SpatialParams;
        using MaterialLaw = typename SpatialParams::MaterialLaw;
        static const auto swRaw = MaterialLaw::sw(this->spatialParams().fractureMaterialLawParams(), pc);
        static const auto swClamped = std::clamp(swRaw, 0.0, 1.0);

        std::cout << "Computed initial fracture water saturation of " << swRaw
                  << ", which corresponds to a capillary pressure of " << pc << ".\n"
                  << "Clamped the saturation to " << swClamped << std::endl;

        return 1.0 - swClamped;
    }

    //! returns true if a position is at the top outflow boundary
    bool isOnTopBoundary_(const GlobalPosition& globalPos) const
    { return globalPos[2] > this->gridGeometry().bBoxMax()[2] - 1e-6; }

    //! returns true if a position is located at the well (assumes the position to be on boundary)
    bool isOnWell_(const GlobalPosition& globalPos) const
    {
        using std::abs;
        const auto y = globalPos[1];
        const auto absY = abs(y);

        if (absY > injectionLength_/2.0 + 1e-6)
            return false;

        const auto distToWellCenterLine = (globalPos - GlobalPosition({0.0, y, 0.0})).two_norm();

        // this statement excludes the bottom and top faces of the well
        // however, for box we have to make sure that the vertices on the
        // rim are included as part of the injection well
        if (abs(absY - injectionLength_/2.0) < 1e-6)
            return abs(distToWellCenterLine - wellRadius_) < 1e-6;
        else
            return distToWellCenterLine < wellRadius_ + 1e-6;
    }

    std::shared_ptr<CouplingManager> couplingManagerPtr_;

    Scalar fracAperture_;
    Scalar faultAperture_;

    Scalar aquiferThickness_;
    Scalar aquiferPermeability_;

    Scalar injectionPressure_;
    Scalar injectionLength_;
    Scalar wellRadius_;
    Scalar time_;

    bool considerMethane_;
    bool considerInjection_;
    bool useZeroInitialCH4Saturation_;
};

} // end namespace Dumux

#endif
