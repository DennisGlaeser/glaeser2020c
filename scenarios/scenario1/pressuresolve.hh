// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Pressure solver for scenario one to get an estimate for the
 *        initial pressure distribution.
 */
#ifndef DUMUX_DELLOCCA2020_SCENARIO1_PRESSURE_SOLVE_HH
#define DUMUX_DELLOCCA2020_SCENARIO1_PRESSURE_SOLVE_HH

#include <dumux/common/properties.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

namespace Dumux {

template<class BulkTypeTag, class FacetTypeTag, class TestTraits, class GridManager>
void pressureSolve(std::shared_ptr<GetPropType<BulkTypeTag, Properties::GridGeometry>> bulkFvGridGeometry,
                   std::shared_ptr<GetPropType<FacetTypeTag, Properties::GridGeometry>> facetFvGridGeometry,
                   GridManager& gridManager,
                   double dtInitial, double tEnd,
                   typename TestTraits::MDTraits::SolutionVector& x)
{
    std::cout << "\n\n\033[1;31mDoing pressure solve to get a pressure distribution estimate\033[0m\n";

    // get grid data for fractures
    auto bulkGridData = gridManager.getGridData()->template getSubDomainGridData<0>();
    auto facetGridData = gridManager.getGridData()->template getSubDomainGridData<1>();

    // the coupling mapper
    auto couplingMapper = std::make_shared<typename TestTraits::CouplingMapper>();
    couplingMapper->update(*bulkFvGridGeometry, *facetFvGridGeometry, gridManager.getEmbeddings());

    // the coupling manager
    using CouplingManager = typename TestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the problems (boundary conditions)
    using BulkProblem = GetPropType<BulkTypeTag, Properties::Problem>;
    using FacetProblem = GetPropType<FacetTypeTag, Properties::Problem>;
    auto bulkSpatialParams = std::make_shared<typename BulkProblem::SpatialParams>(bulkFvGridGeometry, bulkGridData);
    auto bulkProblem = std::make_shared<BulkProblem>(bulkFvGridGeometry, bulkSpatialParams, couplingManager);
    auto facetSpatialParams = std::make_shared<typename FacetProblem::SpatialParams>(facetFvGridGeometry, facetGridData);
    auto facetProblem = std::make_shared<FacetProblem>(facetFvGridGeometry, facetSpatialParams, couplingManager);

    // neglect methane (pure pressure solve)
    bulkProblem->setConsiderMethane(false);
    facetProblem->setConsiderMethane(false);

    // the solution vector
    using MDTraits = typename TestTraits::MDTraits;

    // apply initial solution
    static const auto bulkId = typename MDTraits::template SubDomain<0>::Index();
    static const auto facetId = typename MDTraits::template SubDomain<1>::Index();
    couplingManager->init(bulkProblem, facetProblem, couplingMapper, x);
    auto xOld = x;

    // the grid variables
    using BulkGridVariables = GetPropType<BulkTypeTag, Properties::GridVariables>;
    using FacetGridVariables = GetPropType<FacetTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkFvGridGeometry);
    auto facetGridVariables = std::make_shared<FacetGridVariables>(facetProblem, facetFvGridGeometry);
    bulkGridVariables->init(x[bulkId]);
    facetGridVariables->init(x[facetId]);

    using TimeLoop = TimeLoop<double>;
    auto timeLoop = std::make_shared< TimeLoop >(/*startTime*/0.0,
                                                 dtInitial,
                                                 tEnd);

    // the assembler
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(bulkProblem, facetProblem),
                                                  std::make_tuple(bulkFvGridGeometry, facetFvGridGeometry),
                                                  std::make_tuple(bulkGridVariables, facetGridVariables),
                                                  couplingManager, timeLoop, xOld);

    // solve the system
    using LinearSolver = BlockDiagAMGBiCGSTABSolver;
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;

    auto linearSolver = std::make_shared<LinearSolver>();
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    std::cout << "\n\033[1;34mSolving a few time steps to get stationary conditions\033[0m\n";
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // solve and update
        newtonSolver->solve(x, *timeLoop);
        bulkGridVariables->advanceTimeStep();
        facetGridVariables->advanceTimeStep();
        timeLoop->advanceTimeStep();
        timeLoop->reportTimeStep();
        xOld = x;

        // write current solution to disk
        using BulkGG = GetPropType<BulkTypeTag, Properties::GridGeometry>;
        using FacetGG = GetPropType<FacetTypeTag, Properties::GridGeometry>;
        Dune::VTKWriter< typename BulkGG::GridView> bulkWriter(bulkFvGridGeometry->gridView());
        Dune::VTKWriter< typename FacetGG::GridView> facetWriter(facetFvGridGeometry->gridView());

        std::vector<double> bulkPressure(x[bulkId].size());
        std::vector<double> facetPressure(x[facetId].size());
        for (std::size_t i = 0; i < bulkPressure.size(); ++i) bulkPressure[i] = x[bulkId][i][0];
        for (std::size_t i = 0; i < facetPressure.size(); ++i) facetPressure[i] = x[facetId][i][0];

        if constexpr (BulkGG::discMethod == DiscretizationMethod::box)
            std::cout << "WARNING: Bulk pressure solve output for box not implemented" << std::endl;
        else bulkWriter.addCellData(bulkPressure, "p");

        if constexpr (FacetGG::discMethod == DiscretizationMethod::box)
            std::cout << "WARNING: Facet pressure solve output for box not implemented" << std::endl;
        else facetWriter.addCellData(facetPressure, "p");

        bulkWriter.write("pressuresolve_bulk");
        facetWriter.write("pressuresolve_facet");

        timeLoop->setTimeStepSize(newtonSolver->suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    std::cout << "\033[1;31mFinished pressure solve\033[0m\n";
}


} // end namespace Dumux

#endif
