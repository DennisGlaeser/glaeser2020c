// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the bulk domain in scenario 1
 */
#ifndef DUMUX_DELLOCCA2020_SCENARIO1_BULK_FLOW_PROBLEM_HH
#define DUMUX_DELLOCCA2020_SCENARIO1_BULK_FLOW_PROBLEM_HH

#include <dumux/discretization/cellcentered/tpfa/computetransmissibility.hh>
#include <dumux/discretization/method.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/common/boundarytypes.hh>

namespace Dumux {

/*!
 * \brief The problem for the bulk domain in the two-phase facet coupling test
 */
template<class TypeTag>
class TwoPBulkProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using BoundaryTypes = Dumux::BoundaryTypes<ModelTraits::numEq()>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

    using GridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;

    // post-processing data
    using StringDataPair = std::pair<std::string, Scalar>;
    using StringDataPairVector = std::vector<StringDataPair>;

    // some indices for convenience
    using Indices = typename ModelTraits::Indices;
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx,

        // phase indices
        H2OPhaseIdx = FluidSystem::comp0Idx,
        CH4PhaseIdx = FluidSystem::comp1Idx,

        // first phase conti equation
        contiH2OEqIdx = Indices::conti0EqIdx + H2OPhaseIdx,
        contiCH4EqIdx = Indices::conti0EqIdx + CH4PhaseIdx
    };

public:
    TwoPBulkProblem(std::shared_ptr<const GridGeometry> fvGridGeometry,
                    std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                    std::shared_ptr<CouplingManager> cm)
    : ParentType(fvGridGeometry, spatialParams)
    , couplingManagerPtr_(cm)
    {
        // initialize the tabulation of water properties. Use the default p/T ranges
        using namespace Components;
        using H2O = H2O<Scalar>;
        TabulatedComponent< H2O >::init(273.15, 293.15, 2, 9e4, 1e8, 1000);

        // get values for members and set defaults
        time_ = 0.0;
        aquiferThickness_ = getParam<Scalar>("Problem.AquiferThickness");
        initialCH4Saturation_ = getParam<Scalar>("InitialConditions.InitialMethaneSaturationSource");
        injectionDuration_ = getParam<Scalar>("BoundaryConditions.InjectionDuration");
        injectionPressure_ = getParam<Scalar>("BoundaryConditions.InjectionPressure");
        injectionLength_ = getParam<Scalar>("Problem.InjectionLength");
        wellRadius_ = getParam<Scalar>("Problem.WellRadius");

        considerMethane_ = true;
        considerInjection_ = false;

        // set problem name
        this->setName(getParam<std::string>("Problem.Bulk.Name"));
    }

    //! Specifies the type of boundary condition at a given position
    template<class SubControlEntity>
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlEntity& sce) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        if constexpr (isBox)
        {
            if (considerInjection_ && isOnWell_(sce.dofPosition()))
                values.setDirichlet(contiH2OEqIdx);
        }

        return values;
    }

    //! Specifies the type of interior boundary condition at a given position
    BoundaryTypes interiorBoundaryTypes(const Element& element,
                                        const SubControlVolumeFace& scvf) const
    {
        // fractures are much more permeable, neglect pressure jump
        BoundaryTypes values;
        values.setAllDirichlet();
        return values;
    }

    //! Evaluate the Neumann boundary conditions
    template<class ElementVolumeVariables, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    { return neumann(element, fvGeometry, elemVolVars, scvf); }

    //! Evaluate the Neumann boundary conditions
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector flux(0.0);

        // For TPFA, we incorporate the injection weakly
        if constexpr (!isBox)
        {
            if (isOnWell_(scvf.ipGlobal()) && considerInjection_)
            {
                const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                const auto& insideVolVars = elemVolVars[insideScv];
                const auto& insideK = insideVolVars.permeability();
                const auto extrusion = insideVolVars.extrusionFactor();
                const auto t = -1.0/extrusion
                                   *computeTpfaTransmissibility(scvf, insideScv,
                                                                insideK, extrusion);

                const Scalar pw = insideVolVars.pressure(FluidSystem::phase0Idx);
                const Scalar pInj = injectionPressure_;

                const auto& n = scvf.unitOuterNormal();
                const auto& g = this->spatialParams().gravity(scvf.ipGlobal());
                const auto& rhoW = insideVolVars.density(FluidSystem::phase0Idx);
                const auto& muW = insideVolVars.viscosity(FluidSystem::phase0Idx);
                const auto rhoKng = n*g*insideK*rhoW;

                flux[contiH2OEqIdx] = t*(pInj - pw) + rhoKng;
                flux[contiH2OEqIdx] *= rhoW/muW; // TODO: mobility??
            }
        }

        return flux;
    }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        // domain height
        static constexpr auto facetId = Dune::index_constant<1>();
        const auto& facetGG = couplingManager().problem(facetId).gridGeometry();
        static const Scalar height = facetGG.bBoxMax()[2] + aquiferThickness_;

        // use density at atmospheric conditions for initial pressure distribution
        using FluidState = GetPropType<TypeTag, Properties::FluidState>;
        FluidState fs;
        fs.setPressure(FluidSystem::phase0Idx, 1e5);
        fs.setTemperature(FluidSystem::phase0Idx, temperature());

        const auto rho = FluidSystem::density(fs, FluidSystem::phase0Idx);
        const auto& g = this->spatialParams().gravity(globalPos);

        PrimaryVariables values;
        values[pressureIdx] = 1.0e5 - (height - globalPos[2])*rho*g[2];
        values[saturationIdx] = 0.0;
        if (considerMethane_)
            values[saturationIdx] = initialCH4Saturation_;

        return values;
    }

    //! returns Dirichlet boundary conditions
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        // saturation should never be used
        if (isOnWell_(globalPos))
            return PrimaryVariables({injectionPressure_, 1.0});
        DUNE_THROW(Dune::InvalidStateException, "Dirichlet BCs should only be called at well");
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! update the current time
    void setTime(Scalar t)
    { time_ = t; }

    //! set the initial saturation distribution
    void setInitialSaturation(SolutionVector& x)
    {
        std::cout << "\033[1;33mSetting initial bulk methane saturation\033[0m\n";

        // set initial methane saturation on the dofs inside the source rock
        if (considerMethane_)
        {
            for (const auto& e : elements(this->gridGeometry().gridView()))
            {
                if constexpr (!isBox)
                    x[this->gridGeometry().elementMapper().index(e)][saturationIdx] = initialCH4Saturation_;
                else
                {
                    static constexpr int dim = GridView::dimension;
                    for (unsigned int i = 0; i < e.subEntities(dim); ++i)
                    {
                        const auto& v = e.template subEntity<dim>(i);
                        x[this->gridGeometry().vertexMapper().subIndex(e, i, dim)][saturationIdx] = initialCH4Saturation_;
                    }
                }
            }
        }
    }

    //! set the initial pressure distribution
    void setInitialPressure(SolutionVector& x)
    {
        std::cout << "\033[1;33mSetting initial water pressure distribution\033[0m\n";

        for (const auto& e : elements(this->gridGeometry().gridView()))
        {
            if constexpr (!isBox)
            {
                const auto eIdx = this->gridGeometry().elementMapper().index(e);
                x[eIdx][pressureIdx] = initialAtPos(e.geometry().center())[pressureIdx];
            }
            else
            {
                static constexpr int dim = GridView::dimension;
                for (unsigned int i = 0; i < e.subEntities(dim); ++i)
                {
                    const auto& v = e.template subEntity<dim>(i);
                    const auto vIdx = this->gridGeometry().vertexMapper().subIndex(e, i, dim);
                    x[vIdx][pressureIdx] = initialAtPos(v.geometry().center())[pressureIdx];
                }
            }
        }
    }

    //! Computes post-processing data for .csv output
    template<class BindContextFunc, class GridVariables, class Solution>
    std::vector<std::pair<std::string, Scalar>> getPostProcessingData(BindContextFunc&& bindContext,
                                                                      const GridVariables& gridVars,
                                                                      const Solution& x) const
    {
        using DataPair = std::pair<std::string, Scalar>;
        using Result = std::vector<DataPair>;

        Result result;
        result.emplace_back( std::make_pair("massCH4", 0.0) );

        for (const auto& e : elements(this->gridGeometry().gridView()))
        {
            bindContext(e);
            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());

            fvGeometry.bind(e);
            elemVolVars.bind(e, fvGeometry, x);

            for (const auto& scv : scvs(fvGeometry))
                result[0].second += elemVolVars[scv].saturation(CH4PhaseIdx)
                                    *elemVolVars[scv].density(CH4PhaseIdx)
                                    *elemVolVars[scv].porosity()
                                    *scv.volume();
        }

        return result;
    }

    //! Set if methane should be considered/neglected
    void setConsiderMethane(bool value)
    { considerMethane_ = value; }

    //! Set if the injection should be considered/neglected
    void setConsiderInjection(bool value)
    { considerInjection_ = value; }

private:
    //! returns true if a position is located at the well (assumes the position to be on boundary)
    bool isOnWell_(const GlobalPosition& globalPos) const
    {
        using std::abs;
        const auto y = globalPos[1];
        const auto absY = abs(y);

        if (absY > injectionLength_/2.0 + 1e-6)
            return false;

        const auto distToWellCenterLine = (globalPos - GlobalPosition({0.0, y, 0.0})).two_norm();

        // this statement excludes the bottom and top faces of the well
        // however, for box we have to make sure that the vertices on the
        // rim are included as part of the injection well
        if (abs(absY - injectionLength_/2.0) < 1e-6)
            return abs(distToWellCenterLine - wellRadius_) < 1e-6;
        else
            return distToWellCenterLine < wellRadius_ + 1e-6;
    }

    std::shared_ptr<CouplingManager> couplingManagerPtr_;

    Scalar time_;
    Scalar aquiferThickness_;
    Scalar initialCH4Saturation_;
    Scalar injectionDuration_;
    Scalar injectionPressure_;
    Scalar injectionLength_;
    Scalar wellRadius_;

    bool considerMethane_;
    bool considerInjection_;
};

} // end namespace Dumux

#endif
