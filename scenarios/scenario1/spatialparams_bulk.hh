// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial params for the bulk domain in scenario 1
 */
#ifndef DUMUX_DELLOCCA2020_SCENARIO1_BULK_SPATIAL_PARAMS_HH
#define DUMUX_DELLOCCA2020_SCENARIO1_BULK_SPATIAL_PARAMS_HH

#include <array>
#include <vector>

#include <dumux/io/grid/griddata.hh>
#include <dumux/discretization/method.hh>

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux {

template< class FVGridGeometry, class Scalar >
class TwoPBulkSpatialParams
: public FVSpatialParams< FVGridGeometry, Scalar, TwoPBulkSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = TwoPBulkSpatialParams< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParams< FVGridGeometry, Scalar, ThisType >;

    using GridView = typename FVGridGeometry::GridView;
    using Grid = typename GridView::Grid;
    static constexpr int dim = GridView::dimension;
    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethod::box;

    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    // use a regularized van-genuchten material law
    using EffectiveLaw = RegularizedVanGenuchten<Scalar>;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! export the material law and parameters used
    using MaterialLaw = EffToAbsLaw< EffectiveLaw >;
    using MaterialLawParams = typename MaterialLaw::Params;

    //! the constructor
    TwoPBulkSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                          std::shared_ptr<const Dumux::GridData<Grid>> gridData)
    : ParentType(fvGridGeometry)
    , gridDataPtr_(gridData)
    {
        porosity_ = getParam<Scalar>("SpatialParams.Source.Porosity");
        permeability_  = getParam<Scalar>("SpatialParams.Source.Permeability");
        materialLawParams_.setVgAlpha(getParam<Scalar>("SpatialParams.Source.VGAlpha"));
        materialLawParams_.setVgn(getParam<Scalar>("SpatialParams.Source.VGN"));
        materialLawParams_.setPcHighSw(getParam<Scalar>("SpatialParams.Source.PcHighSw"));
        materialLawParams_.setPcLowSw(getParam<Scalar>("SpatialParams.Source.PcLowSw"));
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template<class ElemSol>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElemSol& elemSol) const
    { return permeability_; }

    //! Return the porosity
    template<class ElemSol>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElemSol& elemSol) const
    { return porosity_; }

    //! Return the material law parameters
    template<class ElemSol>
    const MaterialLawParams& materialLawParams(const Element& element,
                                               const SubControlVolume& scv,
                                               const ElemSol& elemSol) const
    { return materialLawParams_; }

    //! Water is the wetting phase
    template< class FluidSystem >
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        // brine is the first phase in the fluid system
        return FluidSystem::phase0Idx;
    }

    //! Return the material parameters of the source rock
    const MaterialLawParams& materialLawParams() const
    { return materialLawParams_; }

private:
    // pointer to the grid data (contains domain markers)
    std::shared_ptr<const Dumux::GridData<Grid>> gridDataPtr_;

    // parameters for the different domain regions
    Scalar porosity_;
    PermeabilityType permeability_;
    MaterialLawParams materialLawParams_;
};

} // end namespace Dumux

#endif
