// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The properties for the facet domain in scenario1.
 */
#ifndef DUMUX_DELLOCCA2020_SCENARIO1_FACET_FLOW_PROPERTIES_HH
#define DUMUX_DELLOCCA2020_SCENARIO1_FACET_FLOW_PROPERTIES_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/2p/model.hh>

#include "fluidsystems.hh"
#include "problem_facet.hh"
#include "spatialparams_facet.hh"

namespace Dumux::Properties {

// create the type tag node
namespace TTag {
struct TwoPFacet { using InheritsFrom = std::tuple<TwoP>; };

struct TwoPFacetCompressibleTpfa { using InheritsFrom = std::tuple<TwoPFacet, CCTpfaModel>; };
struct TwoPFacetCompressibleMpfa { using InheritsFrom = std::tuple<TwoPFacet, CCTpfaModel>; };
struct TwoPFacetCompressibleBox { using InheritsFrom = std::tuple<TwoPFacet, BoxModel>; };

struct TwoPFacetIncompressibleTpfa { using InheritsFrom = std::tuple<TwoPFacet, CCTpfaModel>; };
struct TwoPFacetIncompressibleMpfa { using InheritsFrom = std::tuple<TwoPFacet, CCTpfaModel>; };
struct TwoPFacetIncompressibleBox { using InheritsFrom = std::tuple<TwoPFacet, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPFacet> { using type = Dune::FoamGrid<2, 3>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPFacet> { using type = TwoPFacetProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPFacet>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

public:
    using type = TwoPFacetSpatialParams<GridGeometry, Scalar>;
};

// pressure-independent permeabilities
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::TwoPFacet>
{ static constexpr bool value = false; };

// the fluid systems
using namespace ScenarioOne;

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPFacetCompressibleTpfa>
{ using type = FluidSystemCompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPFacetIncompressibleTpfa>
{ using type = FluidSystemIncompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPFacetCompressibleMpfa>
{ using type = FluidSystemCompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPFacetIncompressibleMpfa>
{ using type = FluidSystemIncompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPFacetCompressibleBox>
{ using type = FluidSystemCompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPFacetIncompressibleBox>
{ using type = FluidSystemIncompressibleH2O<GetPropType<TypeTag, Properties::Scalar>>; };

} // end namespace Dumux::Properties

#endif
